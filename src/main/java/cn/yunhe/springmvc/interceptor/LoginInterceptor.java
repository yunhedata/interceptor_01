package cn.yunhe.springmvc.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 登陆检测
 * 
 * 注：
 * 流程：
 * 1、访问需要登录的资源时，由拦截器重定向到登录页面；
 * 2、如果访问的是登录页面，拦截器不应该拦截；
 * 3、用户登录成功后，往cookie/session添加登录成功的标识（如用户编号）；
 * 4、下次请求时，拦截器通过判断cookie/session中是否有该标识来决定继续流程还是到登录页面；
 * 5、在此拦截器还应该允许游客访问的资源。
 * 
 * @author YUNHE
 *
 */
public class LoginInterceptor implements HandlerInterceptor {


	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		  
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {       
        
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		
	}

}
